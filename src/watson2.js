//Use the [Speech to Text][speech_to_text] service to recognize the text from a `.flac` file.

//javascript


//
// const heyWatson = require('ibm-watson');
// const { IamAuthenticator } = require('ibm-watson/auth');

// var SpeechToTextV1 = require('watson-speech/speech-to-text/speaker-stream')
var fs = require('fs');

const electron = require('electron');
const path = require('path');
const request = require("request");
const { dialog, app } = electron;
const watson = electron.ipcRenderer;


var formatted;
   var streamIt;

var SpeechToTextV1 = require('watson-developer-cloud/speech-to-text/v1');


// require('dotenv').config();
const WatsonSpeech = require('./scripts/watson-speech/dist/watson-speech.js');//the bundle pack
// const SpeakerStream = require("watson-speech/speech-to-text/speaker-stream");
var FormatStream = require("watson-speech/speech-to-text/format-stream.js")

var fetch = require('./scripts/fetch/dist/fetch.umd.js');


const accessToken = ('eyJraWQiOiIyMDE5MDcyNCIsImFsZyI6IlJTMjU2In0.eyJpYW1faWQiOiJpYW0tU2VydmljZUlkLTgyZTMzZDdmLTRlMDgtNGZhOC05Y2E3LTY1ZGYyNmJhMzMzMSIsImlkIjoiaWFtLVNlcnZpY2VJZC04MmUzM2Q3Zi00ZTA4LTRmYTgtOWNhNy02NWRmMjZiYTMzMzEiLCJyZWFsbWlkIjoiaWFtIiwiaWRlbnRpZmllciI6IlNlcnZpY2VJZC04MmUzM2Q3Zi00ZTA4LTRmYTgtOWNhNy02NWRmMjZiYTMzMzEiLCJzdWIiOiJTZXJ2aWNlSWQtODJlMzNkN2YtNGUwOC00ZmE4LTljYTctNjVkZjI2YmEzMzMxIiwic3ViX3R5cGUiOiJTZXJ2aWNlSWQiLCJ1bmlxdWVfaW5zdGFuY2VfY3JucyI6WyJjcm46djE6Ymx1ZW1peDpwdWJsaWM6c3BlZWNoLXRvLXRleHQ6dXMtc291dGg6YS8yNzgwOTFiOWIyZDA0YzcxOTJlZTJkYzJjNTVjNjVmNTo1M2Q0ZTUzOC05OTI2LTQ0ZGQtOWRlZS1kZjY2MmQ2NTRlODE6OiJdLCJhY2NvdW50Ijp7InZhbGlkIjp0cnVlLCJic3MiOiIyNzgwOTFiOWIyZDA0YzcxOTJlZTJkYzJjNTVjNjVmNSJ9LCJpYXQiOjE1NzE2OTA3NTEsImV4cCI6MTU3MTY5NDM1MSwiaXNzIjoiaHR0cHM6Ly9pYW0uY2xvdWQuaWJtLmNvbS9pZGVudGl0eSIsImdyYW50X3R5cGUiOiJ1cm46aWJtOnBhcmFtczpvYXV0aDpncmFudC10eXBlOmFwaWtleSIsInNjb3BlIjoiaWJtIG9wZW5pZCIsImNsaWVudF9pZCI6ImRlZmF1bHQiLCJhY3IiOjEsImFtciI6WyJwd2QiXX0.XP0dP1eDEZXdv124cim7IqjAyK9W5n_s-z4bPR7UNc5YZ3GZ2KZVqMKEotKWfZ_lmU0VoIzGPwEFVX72GIGZtUmV1VyFvPhasmhKrRnfCTeCgpBQBWxDUxwWuisKYrQwRYxvAstDvZyzcb2YnaggpJzhGq1vHU5V0uRWd7A15em578CsEtyoxjyoFKP1MwSF7G24qmpCjNe5yUc0bXQiBZ7BGH9viwxdX1cCz4fjRVyLxFZNYZeFMWmaMgNSmWTY2SPpWSo31shVpbEUlAQi9l1wPro5b4Nm_t6EW09v03qRZrteHw2hbquy7Amk5ML4_pP1ELLm4w78lGQw415uuQ');

const refreshToken =
('ReVppeaAG5yl_yCAsDRD-1-jAI9cE1t6_HyM6PxNChqI97NI87d7xhF3E58V5Mk7U6AcsGgMRRUwj-l3SiqaWGdsIdsUjxoCnkO9sqtN8WkZsGszcuErhjCHr13TsGVqgdtNTOBAaTVMN7cAoykgGh-Hi6RLHyHmiAG3_m_TwNvciMnkDC5kcT1qQKFU9MRVKHaQSXiNk0OSlPwbpoGC4Pd_dVpBQ_vOcSAk73SQ31eBlZxinVynxxnJBMeDjhkNgc7lrYQmCNdc8Mn6P1abaGcn6DQ8Fwp5n1WDKuGLi7SkUQZgT-rCqFJ_p5njvVS2YXNBzXyHwZNgps_qByyxljA4IQV0mpQ35O2ChaGb6-6wZzg6JPqUxafcx2fJr1ossC-AkOsn-hd310t9i-O_EZwuCfmSNVDad_TaZpHVdgtCQKAJgVmdYhw5p1MoYhAZ2r-graOBevKcKkB8GllCUXZi9nSaje2iY2E6jFU4q807YKlFFCc4oPXh5JlrDvzM71oxHpqQZEbQ92bUDE9ul2_uLYt8GtRVWWphHF3MzILzExzvBmxn3zKxACnS-gMRniLAm-e7B5hqdUWHpbikI17_hJhOLNTWJy9aacaJ53tuehazmpF22x7TjCDZni_N0mLiPA79eS5NC5ovekyilCxXktFp9JR3JyenG9CLIbRtzPbHX-TtoaYTHcbZ6JAnOzVCxhRva1fhef9zNYnOrL2Wdg05-u0Xokgcaxs3TOI9Us11BqrBiYX29kiAdmztYFC7fNpK_8ed9GkvjdQC_xHJNApEmOCmxJYMEPsUz4rrm-v5Ck-6Wf8XGx5luArS7-8qYZq6-LACnE59iLaFrgtJxKb3g_TkwAe277CzL-neAIKAVHtWRSlA4CkH2Ur9otAnDnjdZM5QuEcAYRaoxo8f2N2Lj9GzY3Ld5BfPftYC6s4E51wZeqZZPxX8oNq8Tz4sUnzBSKqecn0Wk5BJRQ95CZ5k62l7DsYKkBVve9ABubKjS-heFo0bJ0i5VCnb1aeCjD-02G3Rl9IxuIuCInbeq1xDgD6fKfjwZTyxjhUe6g');




// to get an IAM Access Token
// const authorization = new watson.AuthorizationV1({
//   authenticator: new IamAuthenticator({ apikey: 'NfzZAcp-11duF8UiODpZOmWnA4DACW23TwSAjoOKEOLG' }),
// });
//
// var theToken;
// authorization.getToken(function (err, token) {
//   if (!token) {
//     console.log('error: ', err);
//   } else {
//       theToken = token;
//       return theToken;
//   }
// });





//hello world message:
console.log('watson.js is loaded');

var audioFile;
var params;
var recognizeStream;
var fileName;
var formatText;
var speakerStreamFormat;
//BUTTON LISTENER:
document.getElementById('button').addEventListener('click',_=>{
  ipcRenderer.send('watsonDebug')
  })





watson.on('watsonGo', function(event, files) {

      var audioFiles = JSON.parse(files); // parse them into a "stringed array"

        runWatson(audioFiles);

    });



//////-----FUNCTIONS-----//////

//Function: Set params-
function runWatson(filepaths) {



  console.log('the file, after being parsed: ' + filepaths);

  //REGISTERING A CALLBACK- Sample from API (the rest is in the for()loop below):
  //https://cloud.ibm.com/apidocs/speech-to-text?code=node#recognize-audio-websockets-

//   const registerCallbackParams = {
//   callback_url: 'http://{user_callback_path}/job_results',
//   user_secret: 'ThisIsMySecret',
// };

      for(var w=0; w<filepaths.length; w++) { // run a loop for each flac filename in the list:



      audioFile = filepaths[w];//rename file in loop


      //Get filename from long filepath string:
       fileName = pullFileName(audioFile);






       // create the stream
           // recognizeStream = speechToText.recognizeUsingWebSocket(params);
           // recognizeStream.pipe(new FormatStream(params));

         //   var speechToText = new SpeechToTextV1({
         //       //the new way to authenticate?
         // //             authenticator: new IamAuthenticator({
         // //   apikey: 'EvsC6zVeSCArkP94ua9XIFGL8r8df63El5ScrxSdJVvd',
         // //   url: 'https://stream.watsonplatform.net/speech-to-text/api/'
         // // }),
         //         iam_apikey: 'EvsC6zVeSCArkP94ua9XIFGL8r8df63El5ScrxSdJVvd',
         //         url: 'https://stream.watsonplatform.net/speech-to-text/api/'
         //         });

         var speechToText = new SpeechToTextV1({
                 iam_apikey: 'EvsC6zVeSCArkP94ua9XIFGL8r8df63El5ScrxSdJVvd',
                 url: 'https://stream.watsonplatform.net/speech-to-text/api/'
                 });


         params = {
              // audio: fs.createReadStream(filePath),
              // token_manager: 'apiToken',
              content_type: 'audio/flac',
              objectMode: true,
              timestamps: true,
              format: true,
              keywords: ['ClassLink', 'Class Link', 'Class Links', 'Analytics', 'One Sync', 'Launch Pad', 'My Files', 'Digital Learning Days', 'Digital Learning Day', 'roster'],
              keywords_threshold: 0.5,
              interim_results: false,
              smart_formatting: true,
              speaker_labels: true,
              inactivity_timeout: 60,
              processing_metrics: true,
              processing_metrics_interval: 0.1
              // watson_token:

              // headers: {
              //    'content-type': 'multipart/form-data',
              //    'transfer-encoding': 'chunked'}
             };


             // params =

         recognizeStream = speechToText.recognizeUsingWebSocket(params);







        processFile(audioFile, fileName);//run the function

            }
    }





//--------FUNCTION: Process file - make the call to Watson------
     function processFile(audioFile, fileName){


        // pipe in some audio





 // audioFile.pipe(recognizeStream).pipe(formatText);




        fs.createReadStream(audioFile).pipe(recognizeStream);







    // OG Listeners:

        recognizeStream.on('data', function(event) {
          onEvent('Data:', event, audioFile);
              // var chunk =

            //
            // var formatted = event.toString()).pipe(formatText);
            // recognizeStream.push(formatted);

        });

        recognizeStream.on('error', function(event) {
          onEvent('Error:', event, audioFile);

        });

        recognizeStream.on('close', function(event) {
          fileDone('Close:', event, audioFile);
        });




          // })
          return fileName;
    }



// FUNCTION: Displays events on the console + Create JSON of transcript:
     function onEvent(name, event,fileName) {

       streamIt = event.toString();
       formatText = new FormatStream({
        accessToken: accessToken,
        refreshToken: refreshToken,
        file: streamIt,
        model: 'en-US_NarrowbandModel',
        resultsBySpeaker: true, // pipes results through a SpeakerStream, and also enables speaker_labels and objectMode
        realtime: false, // don't slow down the results if transcription occurs faster than playback
        format: true,
        play: false,
        speakerlessInterim: false
        });

                   console.log(formatText);

          //THE OG:
           // console.log((JSON.stringify(event,null,2)));

      console.log(name, JSON.stringify(event, null, 2));

      //Create JSON of the transcript
      if (name == 'Close:'){//skip adding the "close = success3000"

      }else{

        fs.writeFile(fileName.replace('.flac', '.json'), JSON.stringify(event, null, 2), (err) => {
        if (err) throw err; //FYI - var params = full filepath. Used to create .json file here (where .flac file lives):

        //TESTING: Close the connection so it doesn't hang:
          //   var closeConn = {
          // action: 'stop'
          // };
          // recognizeStream.send(JSON.stringify(closeConn));



        //Log success to console:
        console.log(fileName +' Transcript file was generated! (using "writeFile" method)');


              })


              }
          }



     function fileDone(name, event, params) {
                var flacFileName = pullFileName(params);
                flacFileName = flacFileName.replace('.flac', '.json');

          //send message to main.js to relay to mainWindow:
            watson.send('fileDone', flacFileName);


          }
    //Function: Get filename from long filepath string:
      function pullFileName(str) {
                return str.split('\\').pop().split('/').pop();
                    }






// function debugTesting(){
//
//
//           console.log('made it!');
//
//         var options = { method: 'GET',
//           url: 'https://gateway-a.watsonplatform.net/speech-to-text/api/v1/recognitions',
//           iam_apikey: 'HhHvL4ltDq9DfWj-dP9td_lL8TF3amxRmRyB7AVnlIST',
//           headers: { 'content-type': 'application/json' } };
//
//         request(options, function (error, response, body) {
//           if (error) throw new Error(error);
//
//           console.log(body);
//         });
//
//     }
