const electron = require('electron')
const fs = require('fs')
const path = require('path')
const { dialog, app, shell } = electron;
// const shell = require('electron').shell;

// const app = electron.app
const BrowserWindow = electron.BrowserWindow
const ipcMain = electron.ipcMain
const PORT = 8080

let tmp_values = []; //declare object
var output = '';

let mainWindow
let watson


//on 'ready', launch the app's window:
app.on('ready', _ => {



      mainWindow = new BrowserWindow({
        height: 1200,
        width: 550,
        // useContentSize: true,
        // maximize: true,
        x: 1400,
        y: 1400,
        webPreferences: {nodeIntegration: true}
        });

      mainWindow.loadURL(`file://${__dirname}/main.html`)
      // mainWindow.loadURL(`file://${__dirname}/server2.html`)

      //garbage collect mainWindow on quit:
        mainWindow.on('closed', _=> {
        mainWindow = null
        })

        console.log('main.js ready!');


        //Function: open DevTools:
            mainWindow.webContents.openDevTools();


    //Launch Watson Window:
      // watsonWindow();//programmed to launch after FLAC files selected by user


    //Launch Server Window :
      // server2Window();

console.log(process.versions);

  }) //^^end of "on('ready')" function



// -------@LISTENERS----- //

//BUTTON: on 'button click' ipcRenderer sends 'clickstart' here to initiate 'selectFiles' function:
ipcMain.on('clickstart',function(event,arg){
        selectFiles(event, arg);
    });

//BUTTON: on 'button click' wipe JSON data from memory to start fresh:
ipcMain.on('clearList', function(evt, data){
    clearJsonData();
    });

//BUTTON: on 'button click' deploy the Watson Script!:
ipcMain.on('callWatson', function(evt, theShit){
      // Deploy Watson!
        theShit = '';
        theShit = output;
        // console.log(theShit);
        //call watson.js to process selected files:
        mainWindow.webContents.send('watsonGo', theShit);
        mainWindow.webContents.send('sentSoClear', theShit)

        });



//SUBMIT BUTTON: after district folders created (via airTableJS), open them up:
ipcMain.on('openDir', function(evt, data){
          shell.openItem(data);
      });




//Return Messages from Watson (& relaying to mainWindow) //
    ipcMain.on('fileDone', function(evt, data) {
          mainWindow.webContents.send('fileDone', data)
          })



//WATSON BUTTON LISTENER:
    ipcMain.on('watsonDebug', function(evt, data) {
          mainWindow.webContents.send('watsonInfo', data)
          })






// -------@FUNCTIONS----- //




        //Launch Watson window
  function watsonWindow(){
  watson = new BrowserWindow({
    height: 500,
    width: 450,
    // useContentSize: true,
    x: 1,
    y: 400
    // webPreferences: {nodeIntegration: true}
    });

  watson.loadURL(`file://${__dirname}/watson.html`)

  //garbage collect mainWindow on quit:
    watson.on('closed', _=> {
    watson = null
    })

    // console.log('watson.js ready!');

    //Function: open DevTools:
        watson.webContents.openDevTools();
}



        //Launch server2 window
  function server2Window(){
  server2 = new BrowserWindow({
    height: 800,
    width: 1200
    // webPreferences: {nodeIntegration: true}
    });

  server2.loadURL(`file://${__dirname}/server2.html`)

  //garbage collect mainWindow on quit:
    server2.on('closed', _=> {
    server2 = null
    })

    // console.log('watson.js ready!');

    //Function: open DevTools:
        server2.webContents.openDevTools();
}


function serverWindow() {
  var serverWindow = new BrowserWindow({ show: true });
  serverWindow.loadURL(`file://${__dirname}/server.html`)


  serverWindow.webContents.openDevTools()
  serverWindow.webContents.once("did-finish-load", function () {
    var http = require("http");
    var server = http.createServer(function (req, res) {
      console.log(req.url)
      if (req.url == '/123') {
        res.end(`ah, you send 123.`);
      } else {
        const remoteAddress = res.socket.remoteAddress;
        const remotePort = res.socket.remotePort;
        res.end(`Your IP address is ${remoteAddress} and your source port is ${remotePort}.`);
      }
    });
    server.listen(PORT);
    console.log("http://localhost:"+PORT);
    // console.log(`Your IP address is ${remoteAddress} and your source port is ${remotePort}.`);


})
}


// Clear JSON Data from Memory
  function clearJsonData(){
    tmp_values = [];
    output = '';
    stringIt = '';
    fs.writeFile('selectedFiles.json', '', (err) => {
    if(err) throw err;
            });


  }

    // Auto-open the DevTools:
  function devTools(windowName){
  windowName.webContents.openDevTools();
  }





// SelectFiles:
function selectFiles(event, arg){
const options = {
    title: 'Open a File or Folder',
    defaultPath: '~/Desktop/Watson-interviews/',
    buttonLabel: 'Do it',
    filters: [
          { name: 'flac', extensions: ['flac'] }
          // { name: 'xml', extensions: ['xml'] }
        ],
    properties: ['openFile', 'multiSelections'],
    message: 'Select Audio FLAC File(s) to Begin Processing with WatsonTron...'
    }

  dialog.showOpenDialog(null, options, (filePaths) => {

          if(!filePaths){
              return;
      }else{


        //run thru the file paths and add to temp array:
        for (var i=0;i<filePaths.length;i++) {

            tmp_values.push(filePaths[i]);
            // console.log(filePaths[i]);
            }



        //strigify temp array into "stringIt" variable:
        var stringIt = JSON.stringify(tmp_values, null, 2);
        console.log(stringIt);
          output = stringIt;
          // tmp_values = [];

        //write strigified file paths to a JSON file:
        fs.writeFile('selectedFiles.json', output, (err) => {
        if(err) throw err;
                });

        //send filepaths to ipcRenderer (browser app window):
          event.sender.send('open-dialog-paths-selected', stringIt)

            // return theShit;
            console.log();

        //open Watson window (if not already open):
        // if(!watson){
        //     watsonWindow();
        //     console.log('watson now active!');
        //   }else{console.log('watson already open.')}




          }
  })

}
