//Tutorial for dialog files at:
// https://www.youtube.com/watch?v=rX3axskesDw


const electron = require('electron')
const fs = require('fs');
const path = require('path')

const watson = electron.ipcRenderer;

// var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

const { dialog, app } = electron
// const ipcRenderer = electron.ipcRenderer;


console.log('renderer.js now active');




// -------@BUTTON LISTENERS----- //
//BUTTON: Event listener for "import button" click:
document.getElementById('button').addEventListener('click',_=>{
  ipcRenderer.send('clickstart')
  })

//BUTTON: Event listener for "clear Files button" click:
document.getElementById('clearButton').addEventListener('click',_=>{
  clearFileList();
  //hide callWatson Button and clearButton since nothing is selected:
  // document.getElementById("callWatson").style.display="none";//callWatson
  // document.getElementById("clearButton").style.display="none";

  })

//BUTTON: Event listener for "callWatson" click:
document.getElementById('callWatson').addEventListener('click',_=>{
  ipcRenderer.send('callWatson', theData)
  })






var theData = '';
//On 'button click', send to ipcMain to activate 'selectFiles' window:
ipcRenderer.on('open-dialog-paths-selected', function(evt, data) {
  //reveal callWatson button:
  document.getElementById("callWatson").style.display="block";
  //reveal clearFlacFiles button:
  document.getElementById("clearButton").style.display="initial";


     theData = JSON.parse(data);

  console.log('Data Parsed:'+ theData);

      //remove all items in fileList to start fresh:
        document.getElementById("output").innerHTML = '<br><h5>Flac Files Ready for Transcribing:</h5>';

        //now, loop thru updated array to add the additional elements:
        for(var k=0; k<theData.length; k++){
          var dataClean = pullFileName(theData[k]);
          document.getElementById('output').innerHTML += "<small>"+ "<li id='bullet'>" + dataClean + "</li>" + "</small>";

      }

  })



//-------DEBUGGING: Turn this back on to clear after sending to watson:
//Return Messages from ipcMain //
  ipcRenderer.on('sentSoClear', function(evt,data){
    clearFileList();
    //remove watson buttons since flac files were sent:
    // document.getElementById("callWatson").style.display="none";//callWatson
    // document.getElementById("clearButton").style.display="none";//clearflac files button

  })




//Return Messages from Watson (relayed from ipcMain) //
  ipcRenderer.on('fileDone', function(evt, data) {
          console.log(data + ' generated.');
        })
//Return Messages from Watson (relayed from ipcMain) //
  ipcRenderer.on('go', function(evt, data) {
          console.log(data + ' hello!');
        })


//Hide Watson buttons at launch:
        // document.getElementById("clearButton").style.display="none";//clearTheFlacFiles
        // document.getElementById("callWatson").style.display="none";//sendWatsonCall









// --------------FUNCTIONS-------------------- //


/*
HOW THE EVENT HANDLER/DYNAMIC DOM ELEMENTS SHIT WAS DONE (SEE BELOW):
Set up a listener for the initial click on [get Frame.io Data] button.
Targeted the 'frameio-content' ghost <div> section from the DOM and added event listener.
THEN, set up a separate function (frameioFolderClick()- see below) to pass in the click event.
THAT'S when we use an "if(){} statement to check for the '.className'. That className is the "text-info" that each new DOM element contains (<li class='text-info'....>).
Yes, the bitch bubbles, but BC of the IF() filter, we don't trigger any other bullshit from occuring
(like spreading down to the dynamically added children (ie-video info that appears onclick))
WHEWWWWWW!!!! Took all of 2 days. emojiface:xnotamused */

//Function for when a frameioFolderName is clicked:
    

// Remove path string to only keep the filename:
function pullFileName(str) {
          return str.split('\\').pop().split('/').pop();
  };

//On 'clear Files button" button2 click' ipcRenderer sends 'clearFiles' here to initiate 'clearFileList' function:
function clearFileList(){
document.getElementById("output").innerHTML = ' ';
ipcRenderer.send('clearList');

};

//run frameioScript @ startup:
// frameio.getIt();
// // .remove();
// fileList.parentNode.removeChild(fileList);
