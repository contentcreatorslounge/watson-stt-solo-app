//Use the [Speech to Text][speech_to_text] service to recognize the text from a `.flac` file.

//javascript
var SpeechToTextV1 = require('watson-developer-cloud/speech-to-text/v1');
// const { IamAuthenticator } = require('ibm-watson/auth');

var FormatStream = require("watson-speech/speech-to-text/format-stream")

var fs = require('fs');

const electron = require('electron');
const path = require('path');
const request = require("request");

const { dialog, app } = electron;
const watson = electron.ipcRenderer;




//hello world message:
console.log('watson.js is loaded');

var audioFile;
var params;
var recognizeStream;
var fileName;

//BUTTON LISTENER:
document.getElementById('button').addEventListener('click',_=>{
  ipcRenderer.send('watsonDebug')
  })


watson.on('watsonGo', function(event, files) {

      var audioFiles = JSON.parse(files); // parse them into a "stringed array"

        runWatson(audioFiles);

    });



//////-----FUNCTIONS-----//////

//Function: Set params-
function runWatson(filepaths) {



  console.log('the file, after being parsed: ' + filepaths);

      for(var w=0; w<filepaths.length; w++) { // run a loop for each flac filename in the list:

          var speechToText = new SpeechToTextV1({
              //credentials 1 - jon@ccl.com (login-jon@ccl. confused on the billing structure)
              // iam_apikey: 'fZVX4MHeSX269ifwuXsKMPtJGgrZkWddBL_6af1s0Kjm',
              // url: 'https://stream.watsonplatform.net/speech-to-text/api/'
              // });

          //*NOTE: credentials 2 = First 500 minutes free:
                    iam_apikey: '9Ty_ippLHkzaHr2HIkSSthVyqzSajY8TtDUN27ECfTV4',
                    url: 'https://api.us-south.speech-to-text.watson.cloud.ibm.com/instances/c68e48ea-2971-4004-b4be-814a63c04629'
                    });

                  params = {
                       // audio: fs.createReadStream(filePath),
                       // token_manager: 'apiToken',
                       content_type: 'audio/flac',
                       objectMode: true,
                       timestamps: true,
                       format: true,
                       processing_metrics: true,
                       processing_metrics_interval: 0.25,
                       keywords: ['Neenah', 'K-12', 'K through 12', 'ClassLink', 'Class Link', 'Class Links', 'Analytics', 'One Sync', 'Launch Pad', 'My Files', 'Digital Learning Days', 'Digital Learning Day', 'roster'],
                       keywords_threshold: 0.2,
                       interim_results: false,
                       smart_formatting: true,
                       // speaker_labels: true,
                       inactivity_timeout: 60,
                       end_of_phrase_silence_time: 0.1,
                       split_transcript_at_phrase_end: true

                       // watson_token:

                       // headers: {
                       //    'content-type': 'multipart/form-data',
                       //    'transfer-encoding': 'chunked'}
                      };

                  params.return_response = true;


      var opts = ({
        objectMode: true,
        type: 'utf8'
      });


       // create the stream
           recognizeStream = speechToText.recognizeUsingWebSocket(params);
           // recognizeStream.pipe(new FormatStream(params));

        audioFile = filepaths[w];//rename file in loop

        processFile(params, audioFile);//run the function

            }
    }




//FUNCTION: Process file - make the call to Watson
     function processFile(params, audioFile){
  // return new Promise(resolve =>{

    //Get filename from long filepath string:
     fileName = pullFileName(audioFile);


     // options = {
     //   file: audioFile,
     //   // only certain models support speaker labels currently,
     //   // see https://console.bluemix.net/docs/services/speech-to-text/output.html#output
     //   model: 'en-US_NarrowbandModel',
     //   resultsBySpeaker: true, // pipes results through a SpeakerStream, and also enables speaker_labels and objectMode
     //   realtime: false, // don't slow down the results if transcription occurs faster than playback
     //   play: false
     // }


        // pipe in some audio
        fs.createReadStream(audioFile).pipe(recognizeStream);




        recognizeStream.on('listening', function(event) {
              console.log('transcribing: '+ pullFileName(audioFile) + '...');

            });
        // recognizeStream.on('message', function(event) {
        //       console.log('good old message info received:' + data);
        //     });
        recognizeStream.on('openingMessage', function(event) {
              console.log(event);
            });
        // recognizeStream.on('finish', function(event) {
        //       console.log(event);
        //     });




    // OG Listeners:

        recognizeStream.on('data', function(event) {
          onEvent('Data:', event, audioFile);
        });

        recognizeStream.on('error', function(event) {
          console.log(error);
          onEvent('Error:', event, audioFile);

        });

        recognizeStream.on('close', function(event) {
          fileDone('Close:', event, audioFile);

        });
          return fileName;
        }



// FUNCTION: Displays events on the console + Create JSON of transcript:
   function onEvent(name, event, params, opts) {

        //log event in console (beautified)
      console.log(name, JSON.stringify(event, null, 2));


//TODO: Fix this so it runs the "formatResult" helper function from the format-stream script:
      var hello = new FormatStream();
      var pleaseWork = hello.formatResult(event);
      console.log("formatted version: ", pleaseWork);


      //Create JSON of the transcript
      if (name == 'Close:'){//skip adding the "close = success3000"

      }else{

        fs.writeFile(params.replace('.flac', '.json'), JSON.stringify(pleaseWork, null, 2), (err) => {
        if (err) throw err; //FYI - var params = full filepath. Used to create .json file here (where .flac file lives):


        //Log success to console:
        console.log(fileName +' Transcript file was generated! (using "writeFile" method)');
               })


             }



        if (name == 'Error:'){

                console.log(event);

            }
      }



     function fileDone(name, event, params) {
                var flacFileName = pullFileName(params);
                flacFileName = flacFileName.replace('.flac', '.json');
            console.log('watson has finished with ' + flacFileName);

          //send message to main.js to relay to mainWindow:
            watson.send('fileDone', flacFileName);


          }
    //Function: Get filename from long filepath string:
      function pullFileName(str) {
                return str.split('\\').pop().split('/').pop();
                    }
